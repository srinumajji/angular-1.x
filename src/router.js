app.config(function($stateProvider) {

  $stateProvider.state({
    name: 'list',
    url: '/list',
	templateUrl: 'src/views/table.html',
	controller :'MainCtroller'
  }).state({
    name: 'add',
    url: '/add',
	templateUrl: 'src/views/add.html',
	controller :'AddCtroller'
  })
  .state( {
    name: 'edit',
    url: '/edit/:id',
    templateUrl: 'src/views/edit.html',
	controller :'EditCtroller'
  });
});