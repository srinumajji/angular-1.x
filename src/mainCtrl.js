var app = angular.module('app1',['ui.router']);
app.controller('MainCtroller',function($scope,empFactory){
	//sample employee json data
	function init(){
		$scope.employeeList = empFactory.getEmps();
		console.log($scope.employeeList);
		
	}
	init();
	$scope.selectedRecord = function(id){
		$scope.id = id;
	};
	$scope.deleteEmp = function(){
		empFactory.deleteEmp($scope.id);
		init();
	};
	
});


app.controller('AddCtroller',function($scope,$location,empFactory){
	$scope.addEmployee = function(){
		empFactory.addEmp($scope.emp);
		$scope.cancel();
	};
	$scope.cancel = function(){
		$location.path('/list');
	};
});


app.controller('EditCtroller',function($scope,$location,$stateParams,empFactory){
	var id = $stateParams.id || $scope.cancel();
	$scope.emp = empFactory.getEmpDetails(id);
	$scope.editEmployee = function(){
		empFactory.editEmp($scope.emp);
		$scope.cancel();
	};
	$scope.cancel = function(){
		$location.path('/list');
	};
});


app.factory('empFactory',function(){
	var listEmps = [
							{   id : 1,
							    location :'vizag',
								firstName : 'John',
								lastName  :'Doe',
								email :'john@example.com'
							},
							{
								id:2,
								location:'Hyderabad',
								firstName : 'Mary',
								lastName  :'Dooley',
								email :'mary@example.com'
							},
							{
								id:3,
								location:'Mumbai',
								firstName : 'July',
								lastName  :'Dooley',
								email :'july@example.com'
							}
	
						];
	var factory = {
		getEmps : getEmps,
		addEmp:addEmp,
		editEmp:editEmp,
		deleteEmp : deleteEmp,
		getEmpDetails : getEmpDetails
	}
	
	return factory;
	
	function getEmps(){
	  return listEmps;	
	};
	function getEmpDetails(id){
		  var emp;
		  angular.forEach(listEmps,function(value,key){
			 if(value.id == id){
				  emp = value;
			  }
		  });	
		  return emp;
		};
	function addEmp(emp){
		emp.id = listEmps.length+1;
		
		listEmps.push(emp);
	}
	function editEmp(emp){
		
		angular.forEach(listEmps,function(value,key){
			
			 if(value.id == emp.id){
				  valu = emp;
			  }
		  });
	}
	function deleteEmp(id){
		
		angular.forEach(listEmps,function(value,key){
			
			 if(value.id == id){
				delete listEmps[key];
				console.log(listEmps);
				   }
		  });
	}
	
});

